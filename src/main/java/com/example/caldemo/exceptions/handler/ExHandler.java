package com.example.caldemo.exceptions.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExHandler {

    @ExceptionHandler(Throwable.class)
    ModelAndView handleGeneralException(Throwable throwable) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("message","You're unauthorized to access calendar events.");
        modelAndView.setViewName("error/unauthorized");
        return modelAndView;
    }

}
