package com.example.caldemo.domains.events.controllers;

import com.example.caldemo.domains.events.services.EventService;
import com.google.api.services.calendar.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@Controller
public class EventController {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("/events")
    private String events(OAuth2AuthenticationToken authentication,
                          Model model) throws IOException, GeneralSecurityException {

        List<Event> items = this.eventService.findEvents(authentication);

        model.addAttribute("events", items);
        return "events/all";
    }


}
