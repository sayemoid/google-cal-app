package com.example.caldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaldemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CaldemoApplication.class, args);
    }

}
